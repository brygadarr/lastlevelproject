# TwójTrener

The aim of our application is to help users choose the best personal trainer. It gives the possibility of searching and filtering offers, which will make it much easier to examine the available options. It is also an excellent advertising platform for trainers who are trying to expand their customer base.